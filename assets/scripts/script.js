window.onload = function() {
  addListenerAll('click', '.mainNavigation', function() { //Главная навигационная панель
    switch (this.getAttribute('id')) {
      case 'registrationButton':
        showPage('registration');
      break;
      case 'formButton':
        alert('На эту страницу вы перейдёте автоматически после выбора способа регистрации.');
      break;
      default:
        alert('Эта кнопка никуда не ведёт.');
      break;
    }
  });

  addListenerAll('click', '#nextRegistration', function() {  //Кнопка Далее в регистрации
    if (document.getElementById('personalRegistration').checked) {
      personsFactory.clearData();
      clearForm();
      switchForm('form');
      showPage('form');
    }
    else {
      personsFactory.clearData();
      clearForm();
      loadCSV();
    }
  });

  addListenerAll('click', '.content > footer > button.submit', function() { //Кнопка Сохранить на форме
    var element = document.querySelectorAll('.tabs.active');
    if (element.length == 1) {
      element = element[0].getAttribute('id');
      switch (element) {
        case 'tabPersonal':
          if (readANDwriteData('personal')) switchForm('creditCard');
        break;
        case 'tabCard':
          if (readANDwriteData('creditCard')) {
            buildTable();
            switchForm('result');
          }
        break;
        case 'tabResult':
          sendData();
        break;
      }
    }
  });

  addListenerAll('click', '.content > footer > button.back', function() { //Кнопка Назад на форме
    var element = document.querySelectorAll('.tabs.active');
    if (element.length == 1) {
      element = element[0].getAttribute('id');
      switch (element) {
        case 'tabCard':
          switchForm('form');
        break;
        case 'tabResult':
          let persons = personsFactory.getData();
          localStorage.singleTemp = JSON.stringify(persons[0]);
          loadData();
          switchForm('creditCard');
        break;
      }
    }
  });

  addListenerAll('click', '.chat > footer > button', function() { //Кнопка чата
    var text = document.getElementById('messageChat').value;
    chat.UserSentMessage({text: text});
    document.getElementById('messageChat').value = '';
  });

  addListenerAll('keyup', '#messageChat', function(event) { //Enter чата
    if (event.keyCode == 13) {
      var text = this.value;
      chat.UserSentMessage({text: text});
      this.value = '';
    }
  });

  addListenerAll('click', '.dropDown > input', function() { //Клик по выпадающему списку
    if (this.getAttribute('disabled') != 'true' && this.parentElement.getElementsByTagName('section').length == 1) {
      this.parentElement.getElementsByTagName('section')[0].style.display = 'block';
      let labels = this.parentElement.querySelectorAll('section > label');
      for (let i = 0; i < labels.length; i += 1) labels[i].style.display = 'block';
    }
  });

  addListenerAll('keyup', '.dropDown > input', function() { //Фильтрация выпадающего списка
    if (this.getAttribute('disabled') != 'true' && this.parentElement.getElementsByTagName('section').length == 1) {
      this.parentElement.getElementsByTagName('section')[0].style.display = 'block';
      let labels = this.parentElement.querySelectorAll('section > label');
      for (let i = 0; i < labels.length; i += 1) {
        if (labels[i].innerText.toUpperCase().indexOf(this.value.toUpperCase()) == -1) labels[i].style.display = 'none';
        else labels[i].style.display = 'block';
      }
    }
  });

  initializeDropDownList(); //Инициализация элементов выпадающего списка

  addListenerAll('blur', '.dropDown > input', function() { //Завершение взаимодействия с выпадающим списком
    if (this.parentElement.querySelectorAll('section').length == 1) this.parentElement.getElementsByTagName('section')[0].style.display = 'none';
  });

  setTimeout(chat.sendAnswerToUser, 3000); //Отправка Приветствия в чате через 3 секунды

  addListenerAll('click', '.sexButton', function() { //Кнопки выбора пола
    let sex = this.getAttribute('id');
    if (sex == 'formMale') setSex('m');
    else setSex('f');
    document.getElementById('formBrand').value = '';
    document.getElementById('formBrand').classList.remove('error');
    document.getElementById('formBrandRequired').style.display = 'none';
    document.getElementById('formPhone').value = '';
    document.getElementById('formPhone').style = 'background: url("assets/imgs/world-flag.png"); background-repeat: no-repeat; background-position: 5px; background-size: 14px;';
    document.getElementById('formPhone').classList.remove('error');
    document.getElementById('formPhoneRequired').style.display = 'none';
  });

  addListenerAll('blur', '#formName', function() { //Мгновенная проверка поля
    let result = personsFactory.checkFields({
      firstname: this.value.trim()
    });
    colorizeField('name', result.firstname);
  });

  addListenerAll('blur', '#formSurname', function() { //Мгновенная проверка поля
    let result = personsFactory.checkFields({
      lastname: this.value.trim()
    });
    colorizeField('surname', result.lastname);
  });

  addListenerAll('blur', '#formPatronymic', function() { //Мгновенная проверка поля
    let result = personsFactory.checkFields({
      patronymic: this.value.trim()
    });
    colorizeField('patronymic', result.patronymic);
  });

  addListenerAll('blur', '#formBirth', function() { //Мгновенная проверка поля
    let tempDate = this.value.split('-');
    let result = personsFactory.checkFields({
      birthday: tempDate[2] + '.' + tempDate[1] + '.' + tempDate[0]
    });
    colorizeField('birthday', result.birthday);
  });

  addListenerAll('blur', '#formCountry', function() { //Мгновенная проверка поля
    let result = personsFactory.checkFields({
      country: this.value.trim()
    });
    colorizeField('country', result.country);
  });

  addListenerAll('blur', '#formAddress', function() { //Мгновенная проверка поля
    let result = personsFactory.checkFields({
      address: this.value.trim()
    });
    colorizeField('address', result.address);
  });

  addListenerAll('blur', '#formMother', function() { //Мгновенная проверка поля
    let result = personsFactory.checkFields({
      mother: this.value.trim()
    });
    colorizeField('mother', result.mother);
  });

  addListenerAll('blur', '#formBankCode', function() { //Мгновенная проверка поля
    let result = personsFactory.checkFields({
      bank: this.value.trim()
    });
    colorizeField('bank', result.bank);
  });

  addListenerAll('blur', '#formHowFind', function() { //Мгновенная проверка поля
    let result = personsFactory.checkFields({
      find: this.value.trim()
    });
    colorizeField('find', result.find);
  });

  addListenerAll('blur', '#formFriendEmail', function() { //Мгновенная проверка поля
    let result = personsFactory.checkFields({
      email: this.value.trim()
    });
    colorizeField('email', result.email);
  });

  addListenerAll('blur', '#formPhone', function() { //Мгновенная проверка поля
    let sex;
    if (document.getElementById('formMale').checked) sex = 'm';
    else sex = 'f';
    document.getElementById('formBrand').removeAttribute('disabled');
    let result = personsFactory.checkFields({
      sex: sex,
      phone: this.value.trim()
    });
    if (!result.phone) {
      document.getElementById('formPhone').classList.add('error');
      document.getElementById('formPhoneRequired').style.display = 'block';
      document.getElementById('formPhone').style = 'background: url("assets/imgs/world-flag.png"); background-repeat: no-repeat; background-position: 5px; background-size: 14px;';
    }
    else {
      document.getElementById('formPhone').classList.remove('error');
      document.getElementById('formPhoneRequired').style.display = 'none';
      if (checkBelPhone(this.value.trim())) document.getElementById('formPhone').style = 'background: url("assets/imgs/bel-flag.png"); background-repeat: no-repeat; background-position: 5px; background-size: 14px;';
      else {
        document.getElementById('formPhone').style = 'background: url("assets/imgs/world-flag.png"); background-repeat: no-repeat; background-position: 5px; background-size: 14px;';
        if (sex == 'f') {
          document.getElementById('formBrand').setAttribute('disabled', 'true');
          document.getElementById('formBrand').value = 'Tefal';
        }
      }
    }
  });

  addListenerAll('blur', '#formBrand', function() { //Мгновенная проверка поля
    let result = personsFactory.checkFields({
      brand: this.value.trim()
    });
    colorizeField('brand', result.brand);
  });

  addListenerAll('blur', '#cardNumber', function() { //Мгновенная проверка поля
    let result = personsFactory.checkFields({
      number: this.value.trim()
    });
    colorizeField('number', result.number);
  });

  addListenerAll('blur', '#cardValid', function() { //Мгновенная проверка поля
    let result = personsFactory.checkFields({
      valid: this.value.trim()
    });
    colorizeField('valid', result.valid);
  });

  addListenerAll('blur', '#cardCVV', function() { //Мгновенная проверка поля
    let result = personsFactory.checkFields({
      cvv: this.value.trim()
    });
    colorizeField('cvv', result.cvv);
  });
}

function colorizeField(field, health) {
  switch (field) {
    case 'name':
      if (!health) {
        document.getElementById('formName').classList.add('error');
        document.getElementById('formNameRequired').style.display = 'block';
      }
      else {
        document.getElementById('formName').classList.remove('error');
        document.getElementById('formNameRequired').style.display = 'none';
      }
    break;
    case 'surname':
      if (!health) {
        document.getElementById('formSurname').classList.add('error');
        document.getElementById('formSurnameRequired').style.display = 'block';
      }
      else {
        document.getElementById('formSurname').classList.remove('error');
        document.getElementById('formSurnameRequired').style.display = 'none';
      }
    break;
    case 'patronymic':
      if (!health) {
        document.getElementById('formPatronymic').classList.add('error');
        document.getElementById('formPatronymicRequired').style.display = 'block';
      }
      else {
        document.getElementById('formPatronymic').classList.remove('error');
        document.getElementById('formPatronymicRequired').style.display = 'none';
      }
    break;
    case 'birthday':
      if (!health) {
        document.getElementById('formBirth').classList.add('error');
        document.getElementById('formBirthRequired').style.display = 'block';
      }
      else {
        document.getElementById('formBirth').classList.remove('error');
        document.getElementById('formBirthRequired').style.display = 'none';
      }
    break;
    case 'country':
      if (!health) {
        document.getElementById('formCountry').classList.add('error');
        document.getElementById('formCountryRequired').style.display = 'block';
      }
      else {
        document.getElementById('formCountry').classList.remove('error');
        document.getElementById('formCountryRequired').style.display = 'none';
      }
    break;
    case 'address':
      if (!health) {
        document.getElementById('formAddress').classList.add('error');
        document.getElementById('formAddressRequired').style.display = 'block';
      }
      else {
        document.getElementById('formAddress').classList.remove('error');
        document.getElementById('formAddressRequired').style.display = 'none';
      }
    break;
    case 'mother':
      if (!health) {
        document.getElementById('formMother').classList.add('error');
        document.getElementById('formMotherRequired').style.display = 'block';
      }
      else {
        document.getElementById('formMother').classList.remove('error');
        document.getElementById('formMotherRequired').style.display = 'none';
      }
    break;
    case 'bank':
      if (!health) {
        document.getElementById('formBankCode').classList.add('error');
        document.getElementById('formBankCodeRequired').style.display = 'block';
      }
      else {
        document.getElementById('formBankCode').classList.remove('error');
        document.getElementById('formBankCodeRequired').style.display = 'none';
      }
    break;
    case 'find':
      if (!health) {
        document.getElementById('formHowFind').classList.add('error');
        document.getElementById('formHowFindRequired').style.display = 'block';
      }
      else {
        document.getElementById('formHowFind').classList.remove('error');
        document.getElementById('formHowFindRequired').style.display = 'none';
      }
    break;
    case 'email':
      if (!health) {
        document.getElementById('formFriendEmail').classList.add('error');
        document.getElementById('formFriendEmailRequired').style.display = 'block';
      }
      else {
        document.getElementById('formFriendEmail').classList.remove('error');
        document.getElementById('formFriendEmailRequired').style.display = 'none';
      }
    break;
    case 'phone':
      if (!health) {
        document.getElementById('formPhone').classList.add('error');
        document.getElementById('formPhoneRequired').style.display = 'block';
        document.getElementById('formPhone').style = 'background: url("assets/imgs/world-flag.png"); background-repeat: no-repeat; background-position: 5px; background-size: 14px;';
      }
      else {
        document.getElementById('formPhone').classList.remove('error');
        document.getElementById('formPhoneRequired').style.display = 'none';
      }
    break;
    case 'brand':
      if (!health) {
        document.getElementById('formBrand').classList.add('error');
        document.getElementById('formBrandRequired').style.display = 'block';
      }
      else {
        document.getElementById('formBrand').classList.remove('error');
        document.getElementById('formBrandRequired').style.display = 'none';
      }
    break;
    case 'number':
      if (!health) {
        document.getElementById('cardNumber').classList.add('error');
        document.getElementById('cardNumberRequired').style.display = 'block';
      }
      else {
        document.getElementById('cardNumber').classList.remove('error');
        document.getElementById('cardNumberRequired').style.display = 'none';
      }
    break;
    case 'valid':
      if (!health) {
        document.getElementById('cardValid').classList.add('error');
        document.getElementById('cardValidRequired').style.display = 'block';
      }
      else {
        document.getElementById('cardValid').classList.remove('error');
        document.getElementById('cardValidRequired').style.display = 'none';
      }
    break;
    case 'cvv':
      if (!health) {
        document.getElementById('cardCVV').classList.add('error');
        document.getElementById('cardCVVRequired').style.display = 'block';
      }
      else {
        document.getElementById('cardCVV').classList.remove('error');
        document.getElementById('cardCVVRequired').style.display = 'none';
      }
    break;
  }
}

function readANDwriteData(type) {
  switch(type) {
    case 'personal':
      let tempDate = document.getElementById('formBirth').value.split('-');
      let sex;
      if (document.getElementById('formMale').checked) sex = 'm';
      else sex = 'f';
      let data = {
        firstname: document.getElementById('formName').value.trim(),
        lastname: document.getElementById('formSurname').value.trim(),
        patronymic: document.getElementById('formPatronymic').value.trim(),
        birthday: tempDate[2] + '.' + tempDate[1] + '.' + tempDate[0],
        country: document.getElementById('formCountry').value.trim(),
        address: document.getElementById('formAddress').value.trim(),
        mother: document.getElementById('formMother').value.trim(),
        bank: document.getElementById('formBankCode').value.trim(),
        find: document.getElementById('formHowFind').value.trim(),
        email: document.getElementById('formFriendEmail').value.trim(),
        phone: document.getElementById('formPhone').value.trim(),
        brand: document.getElementById('formBrand').value.trim(),
        sex: sex
      };
      let result = personsFactory.checkFields(data);
      colorizeField('name', result.firstname);
      colorizeField('surname', result.lastname);
      colorizeField('patronymic', result.patronymic);
      colorizeField('birthday', result.birthday);
      colorizeField('country', result.country);
      colorizeField('address', result.address);
      colorizeField('mother', result.mother);
      colorizeField('bank', result.bank);
      colorizeField('find', result.find);
      colorizeField('email', result.email);
      colorizeField('phone', result.phone);
      colorizeField('brand', result.brand);
      if (result.firstname && result.lastname && result.patronymic && result.birthday && result.country && result.address && result.mother && result.bank && result.find && result.email && result.phone && result.brand) {
        let singleTemp = getSingleTemp();
        for (let key in data) singleTemp[key] = data[key];
        saveSingleTemp(singleTemp);
        return true;
      }
      else return false;
    case 'creditCard':
      let type;
      if (document.getElementById('cardDebet').checked) type = 'd';
      else type = 'c';
      let dataCard = {
        number: document.getElementById('cardNumber').value.trim(),
        valid: document.getElementById('cardValid').value.trim(),
        cvv: document.getElementById('cardCVV').value.trim(),
        type: type
      };
      let resultCard = personsFactory.checkFields(dataCard);
      colorizeField('number', resultCard.number);
      colorizeField('valid', resultCard.valid);
      colorizeField('cvv', resultCard.cvv);
      if (resultCard.number && resultCard.valid && resultCard.cvv) {
        let singleTemp = getSingleTemp();
        for (let key in dataCard) singleTemp[key] = dataCard[key];
        let newPerson = false;
        let replacementId = singleTemp.id;
        if (replacementId == undefined) newPerson = true;
        let person = personsFactory.createPerson(singleTemp);
        if (person) {
          if (newPerson) personsFactory.saveData([person]);
          else {
            person.id = replacementId;
            personsFactory.setData(person.id, person);
          }
          clearForm();
          return true;
        }
      }
      else return false;
  }
}

function getSingleTemp() { //Получение временного объекта из стора
  var singleTemp = {};
  if (localStorage.singleTemp != undefined) singleTemp = JSON.parse(localStorage.singleTemp);
  return singleTemp;
}

function saveSingleTemp(data) { //Сохранение временного объекта в сторе
  localStorage.singleTemp = JSON.stringify(data);
}

function checkBelPhone(number) {
  let tempNumber = number.replace(/\D/gi, '');
  if (tempNumber.search(/375[0-9]{9}/gi) != -1 && tempNumber.length == 12) return true;
  else return false;
}

function initializeDropDownList() {
  addListenerAll('mousedown', '.dropDown > section > label', function() {
    let drop = this.parentElement.parentElement;
    if (drop.getElementsByTagName('input').length == 1) drop.getElementsByTagName('input')[0].value = this.innerText;
  });
}

function setSex(sex) {
  switch (sex) {
    case 'm':
      document.getElementById('formLabelPhone').innerText = 'Номер телефона своей девушки:';
      document.getElementById('formLabelBrand').innerText = 'За какую команду болеешь:';
      document.getElementById('brands').innerHTML = '<label>Другая</label><label>Ювентус</label><label>Барселона</label><label>Бавария</label><label>Реал Мадрид</label><label>Ливерпуль</label><label>Манчестер Юнайтед</label><label>Арсенал</label>';
      document.getElementById('formBrand').value = '';
      initializeDropDownList();
    break;
    case 'f':
      document.getElementById('formLabelPhone').innerText = 'Номер телефона своего парня:';
      document.getElementById('formLabelBrand').innerText = 'Какую сковороду предпочитаешь:';
      document.getElementById('brands').innerHTML = '<label>Другая</label><label>Tefal</label><label>Bork</label><label>Zepter</label><label>TalleR</label><label>Kukmara</label><label>Rondell</label><label>Vitesse</label>';
      document.getElementById('formBrand').value = '';
      initializeDropDownList();
    break;
  }
}

function counter() {
  var count = 0;

  return {
    next: function() {
      return count++;
    },
    reset: function() {
      count = 0;
    }
  }
}

function addListenerAll(event, elements, addedFunction) {
  var elem = document.querySelectorAll(elements);
  for (var i = 0; i < elem.length; i++) elem[i].addEventListener(event, addedFunction);
}

function showPage(page) {
  switch (page) {
    case 'registration':
      document.getElementById('page').style.setProperty('display', 'none');
      document.getElementById('registration').style.setProperty('display', 'block');
      switchSelection('.mainNavigation', 'id', 'registrationButton', 'current');
    break;
    case 'form':
      document.getElementById('page').style.setProperty('display', 'grid');
      document.getElementById('registration').style.setProperty('display', 'none');
      switchSelection('.mainNavigation', 'id', 'formButton', 'current');
    break;
  }
}

function switchForm(tab) {
  switch (tab) {
    case 'form':
      document.getElementById('resultSection').style.setProperty('display', 'none');
      document.getElementById('creditCardSection').style.setProperty('display', 'none');
      document.getElementById('formSection').style.setProperty('display', 'grid');
      switchSelection('.tabs', 'id', 'tabPersonal', 'active');
      document.getElementById('backButton').style.setProperty('display', 'none');
      document.getElementById('formSection').scrollTop = 0;
    break;
    case 'creditCard':
      document.getElementById('formSection').style.setProperty('display', 'none');
      document.getElementById('resultSection').style.setProperty('display', 'none');
      document.getElementById('creditCardSection').style.setProperty('display', 'grid');
      switchSelection('.tabs', 'id', 'tabCard', 'active');
      document.getElementById('backButton').innerText = 'Личные данные';
      document.getElementById('backButton').style.setProperty('display', 'block');
      document.getElementById('creditCardSection').scrollTop = 0;
    break;
    case 'result':
      document.getElementById('formSection').style.setProperty('display', 'none');
      document.getElementById('creditCardSection').style.setProperty('display', 'none');
      document.getElementById('resultSection').style.setProperty('display', 'block');
      switchSelection('.tabs', 'id', 'tabResult', 'active');
      document.getElementById('backButton').innerText = 'Данные кредитной карты';
      document.getElementById('resultSection').scrollTop = 0;
    break;
  }
}

function switchSelection(navigationGroup, attributeSearchName, attributeNededValue, addClass) {
  var elem = document.querySelectorAll(navigationGroup);
  for (var i = 0; i < elem.length; i++) {
    elem[i].classList.remove(addClass);
    if (elem[i].getAttribute(attributeSearchName) == attributeNededValue) elem[i].classList.add(addClass);
  }
}

function loadCSV() {
  var file = document.getElementById('CSVlistRegistration').files;
  if (file.length > 0)  {
    let reader = new FileReader();
    reader.readAsText(file[0]);
    reader.onload = function() {
      var persons = reader.result.split('\n');
      var loadedPersons = [];
      for (var i = 0; i < persons.length; i++) {
        var tempArray = persons[i].split(';');
        var newPerson = null;
        if (tempArray.length == 17) newPerson = personsFactory.createPerson({
          firstname: tempArray[0].trim(),
          lastname: tempArray[1].trim(),
          patronymic: tempArray[2].trim(),
          birthday: tempArray[3].trim(),
          sex: tempArray[4].trim(),
          country: tempArray[5].trim(),
          address: tempArray[6].trim(),
          mother: tempArray[7].trim(),
          bank: tempArray[8].trim(),
          email: tempArray[9].trim(),
          find: tempArray[10].trim(),
          phone: tempArray[11].trim(),
          brand: tempArray[12].trim(),
          number: tempArray[13].trim(),
          valid: tempArray[14].trim(),
          cvv: tempArray[15].trim(),
          type: tempArray[16].trim()
        });
        if (newPerson) loadedPersons.push(newPerson);
      }

      var promise = new Promise((resolve) => {
        personsFactory.saveData(loadedPersons);
        resolve();
      });
    
      promise.then(function() { 
        if (loadedPersons.length == 0) alert('Ничего не загружено.');
        else {
          if (persons.length > loadedPersons.length) alert('При загрузке из-за ошибок было пропущено ' + (persons.length - loadedPersons.length) + ' строк из ' + persons.length + '.');
          buildTable();
          switchForm('result');
          showPage('form');
        }
      });

      /*personsFactory.saveData(loadedPersons);
      if (loadedPersons.length == 0) alert('Ничего не загружено.');
      else {
        if (persons.length > loadedPersons.length) alert('При загрузке из-за ошибок было пропущено ' + (persons.length - loadedPersons.length) + ' строк из ' + persons.length + '.');
        buildTable();
        addListenerAll('click', '.sortTable', function() {
          buildTable(this.getAttribute('field'));
        });
        switchForm('result');
        showPage('form');
      }*/

    };
    reader.onerror = function() {
      alert('Ошибка открытия файла');
      console.log('Ошибка открытия файла: ' + reader.error);
    };
  }
  else alert('Выберите файл!');
}

function clearForm() {
  setSex('m');
  document.getElementById('formMale').checked = 'true';
  document.getElementById('formBrand').value = '';
  document.getElementById('formBrand').classList.remove('error');
  document.getElementById('formBrandRequired').style.display = 'none';
  document.getElementById('formPhone').value = '';
  document.getElementById('formPhone').style = 'background: url("assets/imgs/world-flag.png"); background-repeat: no-repeat; background-position: 5px; background-size: 14px;';
  document.getElementById('formPhone').classList.remove('error');
  document.getElementById('formPhoneRequired').style.display = 'none';
  document.getElementById('formName').value = '';
  document.getElementById('formName').classList.remove('error');
  document.getElementById('formNameRequired').style.display = 'none';
  document.getElementById('formSurname').value = '';
  document.getElementById('formSurname').classList.remove('error');
  document.getElementById('formSurnameRequired').style.display = 'none';
  document.getElementById('formPatronymic').value = '';
  document.getElementById('formPatronymic').classList.remove('error');
  document.getElementById('formPatronymicRequired').style.display = 'none';
  document.getElementById('formBirth').classList.remove('error');
  document.getElementById('formBirthRequired').style.display = 'none';
  document.getElementById('formCountry').value = '';
  document.getElementById('formCountry').classList.remove('error');
  document.getElementById('formCountryRequired').style.display = 'none';
  document.getElementById('formAddress').value = '';
  document.getElementById('formAddress').classList.remove('error');
  document.getElementById('formAddressRequired').style.display = 'none';
  document.getElementById('formMother').value = '';
  document.getElementById('formMother').classList.remove('error');
  document.getElementById('formMotherRequired').style.display = 'none';
  document.getElementById('formBankCode').value = '';
  document.getElementById('formBankCode').classList.remove('error');
  document.getElementById('formBankCodeRequired').style.display = 'none';
  document.getElementById('formHowFind').value = '';
  document.getElementById('formHowFind').classList.remove('error');
  document.getElementById('formHowFindRequired').style.display = 'none';
  document.getElementById('formFriendEmail').value = '';
  document.getElementById('formFriendEmail').classList.remove('error');
  document.getElementById('formFriendEmailRequired').style.display = 'none';
  document.getElementById('cardNumber').value = '';
  document.getElementById('cardNumber').classList.remove('error');
  document.getElementById('cardNumberRequired').style.display = 'none';
  document.getElementById('cardValid').value = '';
  document.getElementById('cardValid').classList.remove('error');
  document.getElementById('cardValidRequired').style.display = 'none';
  document.getElementById('cardCVV').value = '';
  document.getElementById('cardCVV').classList.remove('error');
  document.getElementById('cardCVVRequired').style.display = 'none';
  localStorage.removeItem('singleTemp');
}

function buildTable(field) {
  var persons = personsFactory.getData();
  var button;
  if (persons.length > 1) {
   document.getElementById('tableHead').innerText = 'Функции';
   button = 'Править';
   document.querySelector('button.creditCardInfo').style.display = 'none';
   document.getElementById('backButton').style.display = 'none';
  }
  else {
    document.getElementById('tableHead').innerText = 'Инфо';
    button = 'Доп.инфо';
    document.querySelector('button.creditCardInfo').style.display = 'block';
    document.getElementById('backButton').style.display = 'block';
  }
  var table = document.getElementById('tableBody');
  table.innerHTML = '';
  if (field != null) persons.sortByField(field);
  for (var i = 0; i < persons.length; i++) {
    var sex;
    if (persons[i].sex == 'm') sex = 'Мужской';
    else sex = 'Женский';
    table.innerHTML += '<label>' + persons[i].lastname + '</label><label>' + persons[i].firstname + '</label><label>' + persons[i].patronymic + '</label><label>' + persons[i].birthday + '</label><label>' + sex + '</label><label>' + persons[i].country + '</label><label><button ident="' + persons[i].id + '">' + button + '</button></label>';
  }
  if (persons.length > 1) initializeTableEditButtons();
  else initializeTableInfoButtons();
  addListenerAll('click', '.sortTable', function() {
    buildTable(this.getAttribute('field'));
  });

  function initializeTableEditButtons() {
    addListenerAll('click', '.table > #tableBody > label > button', function() {
      let id = this.getAttribute('ident');
      let persons = personsFactory.getData();
      localStorage.singleTemp = JSON.stringify(persons[id]);
      loadData();
      switchForm('form');
    });
  }

  function initializeTableInfoButtons() {
    let panel = document.getElementById('infoPanel');
    addListenerAll('click', '.table > #tableBody > label > button', function(event) {
      document.getElementById('creditCardInfoPanel').style.display = 'none';
      document.getElementById('personalInfoPanel').style.display = 'block';
      document.getElementById('personalInfoPanel').innerHTML = getPersonalInfo(persons[0]);
      let top = event.clientY;
      let left = event.clientX;
      panel.style.top = (top - 136) + 'px';
      panel.style.left = (left + 30) + 'px';
      panel.style.display = 'block';
      hideTimer();
    });
    addListenerAll('click', '#resultSection > .creditCardInfo', function(event) {
      document.getElementById('personalInfoPanel').style.display = 'none';
      document.getElementById('creditCardInfoPanel').style.display = 'block';
      document.getElementById('creditCardInfoPanel').innerHTML = getCreditCardInfo(persons[0]);
      let top = event.clientY;
      let left = event.clientX;
      panel.style.top = (top - 136) + 'px';
      panel.style.left = (left + 30) + 'px';
      panel.style.display = 'block';
      hideTimer();
    });
    addListenerAll('blur', '.table > #tableBody > label > button', function() {
      panel.style.display = 'none';
    });
    addListenerAll('blur', '#resultSection > .creditCardInfo', function() {
      panel.style.display = 'none';
    });

    function hideTimer() {
      setTimeout(function() { 
        panel.style.display = 'none'; 
      }, 7000);
    }

    function getPersonalInfo(person) {
      let phoneSex = 'парня';
      let brandSex = 'Какую сковороду предпочитаешь:';
      if (person.sex == 'm') {
        phoneSex = 'девушки';
        brandSex = 'За какую команду болеешь:';
      }
      let info = [
        '<label class="bold">Адрес, почтовый индекс:</label><br>' + person.address,
        '<label class="bold">Девичья фамилия матери:</label><br>' + person.mother,
        '<label class="bold">Кодовое слово в вашем банке:</label><br>' + person.bank,
        '<label class="bold">Как вы узнали о нашем сайте:</label><br>' + person.find,
        '<label class="bold">Email друга:</label><br>' + person.email,
        '<label class="bold">Номер телефона ' + phoneSex + ':</label><br>' + person.phone,
        '<label class="bold"> ' + brandSex + '</label><br>' + person.brand
      ];
      return info.join('<p>');
    }

    function getCreditCardInfo(person) {
      let type = 'Дебетовая';
      if (person.type == 'c') type = 'Кредитная';
      let info = [
        '<label class="bold">Номер карты:</label><br>' + person.number,
        '<label class="bold">Месяц / год:</label><br>' + person.valid,
        '<label class="bold">CVC2 или CVV2:</label><br>' + person.cvv,
        '<label class="bold">Тип карты:</label><br>' + type
      ];
      return info.join('<p>');
    }

    //
  }
}

function loadData() {
  let currentPerson = getSingleTemp();
  document.getElementById('formName').value = currentPerson.firstname;
  document.getElementById('formSurname').value = currentPerson.lastname;
  document.getElementById('formPatronymic').value = currentPerson.patronymic;
  let tempDate = currentPerson.birthday.split('.');
  document.getElementById('formBirth').value = tempDate[2] + '-' + tempDate[1] + '-' + tempDate[0];
  document.getElementById('formCountry').value = currentPerson.country;
  if (currentPerson.sex == 'm') {
    document.getElementById('formMale').checked = 'true';
    setSex('m');
  }
  else {
    document.getElementById('formFemale').checked = 'true';
    setSex('f');
  }
  document.getElementById('formAddress').value = currentPerson.address;
  document.getElementById('formMother').value = currentPerson.mother;
  document.getElementById('formBankCode').value = currentPerson.bank;
  document.getElementById('formHowFind').value = currentPerson.find;
  document.getElementById('formFriendEmail').value = currentPerson.email;
  document.getElementById('formPhone').value = currentPerson.phone;
  document.getElementById('formBrand').removeAttribute('disabled');
  if (checkBelPhone(currentPerson.phone)) document.getElementById('formPhone').style = 'background: url("assets/imgs/bel-flag.png"); background-repeat: no-repeat; background-position: 5px; background-size: 14px;';
  else {
    document.getElementById('formPhone').style = 'background: url("assets/imgs/world-flag.png"); background-repeat: no-repeat; background-position: 5px; background-size: 14px;';
    if (currentPerson.sex == 'f') document.getElementById('formBrand').setAttribute('disabled', 'true');
  }
  document.getElementById('formBrand').value = currentPerson.brand;
  if (currentPerson.type = 'd') document.getElementById('cardDebet').checked = 'true';
  else document.getElementById('cardCredit').checked = 'true';
  document.getElementById('cardNumber').value = currentPerson.number;
  document.getElementById('cardValid').value = currentPerson.valid;
  document.getElementById('cardCVV').value = currentPerson.cvv;
}

function sendData() {
  let persons = personsFactory.getData();
  let stringsArray = [];
  for (let i = 0; i < persons.length; i += 1) {
    let string = '';
    string += persons[i].firstname + ';';
    string += persons[i].lastname + ';';
    string += persons[i].patronymic + ';';
    string += persons[i].birthday + ';';
    string += persons[i].sex + ';';
    string += persons[i].country + ';';
    string += persons[i].address + ';';
    string += persons[i].mother + ';';
    string += persons[i].bank + ';';
    string += persons[i].email + ';';
    string += persons[i].find + ';';
    string += persons[i].phone + ';';
    string += persons[i].brand + ';';
    string += persons[i].number + ';';
    string += persons[i].valid + ';';
    string += persons[i].cvv + ';';
    string += persons[i].type;
    stringsArray.push(string);
  }
  if (confirm('Данные подготовлены к отправке на сервер. Продолжить?')) {
    document.getElementById('exportPersons').value = stringsArray.join('\r\n');
    document.getElementById('exportPersonsForm').submit();
  }
}

class person {
  constructor(options) {
    this.id = options.id;
    this.firstname = options.firstname;
    this.lastname = options.lastname;
    this.patronymic = options.patronymic;
    this.birthday = options.birthday;
    this.sex = options.sex;
    this.country = options.country;
    this.address = options.address;
    this.mother = options.mother;
    this.bank = options.bank;
    this.find = options.find;
    this.email = options.email;
    this.phone = options.phone;
    this.brand = options.brand;
    this.number = options.number;
    this.valid = options.valid;
    this.cvv = options.cvv;
    this.type = options.type;
  }
}

class factory {
  createPerson(options) {
    options.id = counter.next();
    var checkResult = this.checkFields(options);
    var hasError = false;
    for (var value in checkResult) { 
      if (!checkResult[value]) hasError = true; 
    }
    if (hasError) {
      console.log('Ошибка в пользователе: ' + options.lastname + ' ' + options.firstname + ' ' + options.patronymic);
      return checkResult;
    }
    else return new person(options);
  }

  getData() { //Получение списка пользователей из стора
    var persons = false;
    if (localStorage.persons != undefined) persons = JSON.parse(localStorage.persons);
    return persons;
  }

  saveData(data) { //Сохранение списка пользователей в сторе
    localStorage.persons = JSON.stringify(data);
  }

  setData(id, replacementPerson) { //Редактирование одного пользователя
    var currentArray = [];
    if (localStorage.persons != undefined) currentArray = JSON.parse(localStorage.persons);
    for (let i = 0; i < currentArray.length; i += 1) {
      if (currentArray[i].id == id) { 
        currentArray[i] = replacementPerson;
        localStorage.persons = JSON.stringify(currentArray);
        break;
      }
    }
  }

  clearData() { //Очистка списка пользователей
    counter.reset();
    localStorage.clear();
  }

  checkFields(options) {
    var emailReg = /[A-Za-z0-9-_.]+@[A-Za-z.-]+\.[A-Za-z]+/gi;
    var phoneReg = /[+]{0,1}[0-9()\- ]+/gi;
    var belPhoneReg = /375[0-9]{9}/gi;
    var numberReg = /[0-9 ]+/gi;
    var validReg = /\d{2}(\/|\\)\d{2}/gi;
    var digitsReg = /\D/gi;
    var result = {};
    result.firstname = (options.firstname != undefined && options.firstname.length > 0) ? true : false;
    result.lastname = (options.lastname != undefined && options.lastname.length > 0) ? true : false;
    result.patronymic = (options.patronymic != undefined && options.patronymic.length) > 0 ? true : false;
    if (options.birthday == undefined) result.birthday = false;
    else {
      var tempDate = options.birthday.split('.');
      result.birthday = (tempDate.length == 3 && !isNaN(Date.parse(tempDate[1] + '-' + tempDate[0] + '-' + tempDate[2]))) ? true : false;
    }
    result.sex = (options.sex != undefined && (options.sex == 'm' || options.sex == 'f')) ? true : false;
    result.country = (options.country != undefined && options.country.length > 0) ? true : false;
    result.address = (options.address != undefined && options.address.length > 0) ? true : false;
    result.mother = (options.mother != undefined && options.mother.length > 0) ? true : false;
    result.bank = (options.bank != undefined && options.bank.length > 0) ? true : false;
    result.find = (options.find != undefined && options.find.length > 0) ? true : false;
    result.email = (options.email != undefined && options.email.search(emailReg) != -1) ? true : false;
    result.phone = (options.phone != undefined && options.phone.replace(digitsReg, '').length > 7 && options.phone.search(phoneReg) != -1) ? true : false;
    if (result.sex && options.sex == 'm') {
      result.phone = (options.phone != undefined && options.phone.replace(digitsReg, '').length == 12 && options.phone.search(phoneReg) != -1 && options.phone.replace(digitsReg, '').search(belPhoneReg) != -1) ? true : false;
    }
    result.brand = (options.brand != undefined && options.brand.length > 0) ? true : false;
    result.number = (options.number != undefined && options.number.replace(digitsReg, '').length == 16 && options.number.search(numberReg) != -1) ? true : false;
    if (options.valid == undefined) result.valid = false;
    else {
      var tempValid = options.valid.split(/\/|\\/);
      result.valid = (options.valid.search(validReg) != -1 && tempValid[0] * 1 > 0 && tempValid[0] <= 12 && tempValid[1].length == 2) ? true : false;
    }
    result.cvv = (options.cvv != undefined && options.cvv.replace(digitsReg, '').length == 3) ? true : false;
    result.type = (options.type != undefined && (options.type == 'c' || options.type == 'd')) ? true : false;
    return result;
  }
}

Array.prototype.sortByField = function(field) {
  switch (field) {
    case 'lastname':
      this.sort((a, b) => a.lastname > b.lastname ? 1 : -1);
    break;
    case 'firstname':
      this.sort((a, b) => a.firstname > b.firstname ? 1 : -1);
    break;
    case 'patronymic':
      this.sort((a, b) => a.patronymic > b.patronymic ? 1 : -1);
    break;
    case 'birthday':
      this.sort((a, b) => a.birthday > b.birthday ? 1 : -1);
    break;
    case 'sex':
      this.sort((a, b) => a.sex > b.sex ? 1 : -1);
    break;
    case 'country':
      this.sort((a, b) => a.country > b.country ? 1 : -1);
    break;
  }
}

class chatBoard {
  UserSentMessage(options) {
    document.getElementById('chatBoard').innerHTML = '<section class="message user"><section>' + options.text + '</section></section>' + document.getElementById('chatBoard').innerHTML;
    setTimeout(this.sendAnswerToUser, 2000, options);
    document.getElementById('chatBoard').scrollTop = document.getElementById('chatBoard').scrollHeight;
  }

  sendAnswerToUser(options) {
    var answerText
    if (options == null) {
      answerText = 'Здравствуйте! Пишите мне, если у Вас возникнут вопросы по работе сайта.';
    }
    else {
      var variable = Random(1, 3);
      switch (variable) {
        case 1:
          answerText = 'Ok. Просто Ok.'
        break;
        case 2:
          answerText = 'Извините, не понимаю, что Вы имеете в виду...'
        break;
        case 3:
          answerText = 'Это... "' + options.text + '" ...очень интересно, несомненно. Но не могли бы Вы повыторить это?'
        break;
      }
    }
    document.getElementById('chatBoard').innerHTML = '<section class="message operator"><img src="assets/imgs/girl.png" alt="Anna"><section>' + answerText + '</section></section>' + document.getElementById('chatBoard').innerHTML;
    document.getElementById('chatBoard').scrollTop = document.getElementById('chatBoard').scrollHeight;

    function Random(min, max) {
      return Math.round(Math.random() * (max - min) + min);
    }
  }
}

var personsFactory = new factory();
var chat = new chatBoard();
var counter = counter();